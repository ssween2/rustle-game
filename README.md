# rustle-game

A clone of the popular WORDLE game coded in rust
Allows user to play a console version of the game. 

#How It Works
The wordlist was created from a .txt list of legal scrabble words found online. I ran a quick python script to pare that
down to include only five-letter words and to make all of those words all upper case. The rustle game code converts all
user input to all upper case as well for ease of comparison. 
At the start of the game the user is told the rules, which are relatively simple. The game chooses a secret word from 
the word list. Then the user is prompted for input. Their input must be a five-letter word contained in the word list 
or else they are re-prompted. When an appropriate guess is entered the game reprints it with each letter labled by a 
different symbol that reveals some information about the letters in their guess. Letters that are not in the secret word
are labeled with a '_' character. Letters that are in the secret word at a different index are labeled with a '*'.  
Letters that are in the secret word at the same index are labled with a '='. The game runs until the user has entered 
six incorrect guesses or enters the secret word as a guess. At the end of the game the secret word is revealed. 

In order to run it should be as simple as typing "cargo build" and then "cargo run". Each run of the code is one play
through of the game. 

#How It Was Tested
Because of the user-input nature of the game I didn't quite know how to come up with unit tets for each function. Though
I know its not exactly good practice I tested the game mostly just by playing it a bunch. The main problem actual game
play right now is that the word list is not very well curated at all. Turns out a whole lot of junk words are legal in
scrabble, which is where I got the original word list. 

#How It Went
Poorly. It took me a very long time just to put together this simple command-line style game when I originally intended
to learn Yew or some other rust web framework and build an actual clone of the Wordle game that ran in a web browser. 
I didn't give myself enough time to do that and had a hard time dealing with Strings and user input with rust in this
simple form anyways. All of the functionality is there for the most part - it builds a word list, it takes user input, 
it gives the user back information about thier guesses. This is the core game of wordle. I wanted to at least make the 
console output prettier because I know there is a way to use actual unicode emoji characters - those green, yellow, and 
black boxes that have made wordle such a hit on social media. But I couldn't get those to print out on the school linux
servers or on my home computer using windows powershell so the game is just ascii text - information about guesses is 
given in the rudementary form described above. 
