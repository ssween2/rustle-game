///main.rs -- Sean Sweeney
///Includes the main function that creates a new game of wordle and runs play_game

mod lib;
use crate::lib::*;

fn main() {
    let mut new_game = Game::new();
    new_game.play_game();

}
