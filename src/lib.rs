///lib.rs -- Sean Sweeney
///Includes a number of functions for use in worlde clone
///Can play a command-line based version of wordle 

use bracket_random::prelude::RandomNumberGenerator;

///Functions that returns a vector
///containing all the lines in the fiveLetterWords.txt file.
///in which every line is an uppercase
///word, specifically five letter words
pub fn get_wordlist() -> Vec<String> {
    let wordlist: &str = include_str!("fiveLetterWords.txt");
    let wordlist: Vec<&str> = wordlist.split('\n').collect();
    let mut ret_list: Vec<String> = Vec::new();
    for word in wordlist{
        ret_list.push(word.to_string());
    }
    ret_list
}

///Function that takes a wordlist as a reference to a vector of strings
///like the wordlist returned from get_wordlist and a reference to a string
//for the players guess and returns true if the guess is in the wordlist and
///false otherwise
pub fn in_wordlist(wordlist: &Vec<String>, guess: &String) -> bool {
    for word in wordlist {
        if word == guess {
            return true;
        }
    }
    false
}

///Functions that takes a char and a reference to a string and returns true if
///that char is in the string and false otherwise
pub fn char_in_word(to_check: char, word: &String) -> bool {
    for c in word.chars(){
        if to_check == c{
            return true;
        }
    }
    false
}

///Struct to represent a wordle game. Has a field for teh wordlist, the
///secret word, and the guesees the player has made
pub struct Game {
    wordlist: Vec<String>,
    secret_word: String,
    guesses: Vec<String>,
}

//Game struct keeps track of wordlist
//includes functions for getting guesses from the player a
//and displaying the current state of the game
impl Game {

    ///Starts a new game, fills in the wordlist, picks a random entry in the
    ///wordlist to act as the secret word, initilizes guesses vector
    pub fn new() -> Self {
        let wordlist = get_wordlist();
        let mut rng = RandomNumberGenerator::new();
        let secret_word = rng.random_slice_entry(&wordlist).unwrap().clone();
        Self {
            wordlist,
            secret_word,
            guesses: Vec::new(),
        }
    }

    ///Main game logic for a single round of wordle. Prompts user for guesses
    ///and outputs the data for those guesses in a loop until the player has
    ///used up all their guesses or they guess the secret word
    pub fn play_game(&mut self) {
        println!("Thanks for playing command line rustle!");
        println!("Guide to symbols: ");
        println!("_ : the letter in this spot does not appear in the secret word");
        println!("* : the letter in this spot appears in the secret word at a different spot");
        println!("= : the letter in this spot appears at the same spot in the secret word");
        let mut guess = self.get_guess();
        while guess != self.secret_word && self.guesses.len() <6{
            self.print_guess_data(&guess);
            println!("guess again");
            guess = self.get_guess();
        }
        self.print_guess_data(&guess);
        println!("Game over!");
        println!("The word was: {}", self.secret_word);
    }


    ///prompts the user for input and then takes user input and returns the guess
    ///if input is appropriate - rejects and reprompts user if input is not 5
    ///letters long or is not contained in the wordlist
    ///converts input to all uppercase letters for comparison
    pub fn get_guess(&mut self) -> String {
        println!("Enter a guess: ");
        let mut guess = String::new();
        std::io::stdin().read_line(&mut guess).unwrap();
        guess = guess.trim().to_uppercase();
        while guess.len() != 5 && !in_wordlist(&self.wordlist, &guess) {
            println!("invalid guess");
            println!("Enter a guess: ");
            guess = String::new();
            std::io::stdin().read_line(&mut guess).unwrap();
            guess = guess.trim().to_uppercase();
        }
        self.guesses.push(guess.clone());
        guess
    }

    ///Prints out data about a users guess as per the wordle rules
    ///Would prefer to use 🟩, 🟨, and ⬛ but couldn't get themt to print
    ///correctly on windows powershell. uses =, *, and _ respectively, this is
    ///explained in the rules printed by play_game function
    pub fn print_guess_data(&mut self, guess: &String) {
        let mut guess_data: Vec<char> = Vec::new();
        for i in 0..guess.len(){
            if guess.chars().nth(i).unwrap() == self.secret_word.chars().nth(i).unwrap(){
                guess_data.push('=');
            } else if char_in_word(guess.chars().nth(i).unwrap(),&self.secret_word){
                guess_data.push('*');
            } else {
                guess_data.push('_');
            }

        }
        for letter in guess.chars() {
            print!("{} ", letter);
        }
        print!("{}", "\n");

        for c in guess_data {
            print!("{} ", c);
        }
        print!("{}", "\n\n");
    }
}
